export type TQuiz = {
  no: number;
  question: string;
  choices: string[];
  correct_answer: string;
};

export type TRecords = { no: number; answer: string; time: number };
