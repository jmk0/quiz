import produce from "immer";
import { useCallback, useState } from "react";

type TRecords = { no: number; answer: string };

function useRecords(initRecords?: TRecords[]) {
  const [userSolves, setUserSolves] = useState<TRecords[]>(initRecords || []);

  const setUserAnswer = useCallback((no: number, answer: string) => {
    setUserSolves((prevState) =>
      produce(prevState, (draftState) => {
        draftState[no - 1] = { no, answer };
      })
    );
  }, []);

  return {
    userSolves,
    setUserAnswer,
  };
}

export default useRecords;
