import axios from "axios";
import { useCallback, useEffect, useState } from "react";

import type { TQuiz } from "../@type";
import { shuffle } from "../lib/utils";

type TResponseQuiz = {
  category: string;
  correct_answer: string;
  difficulty: string;
  incorrect_answers: string[];
  question: string;
  type: string;
};

function useQuiz(quizCnt: number, initQuizzes?: TQuiz[]) {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [quizzes, setQuizzes] = useState<TQuiz[] | null>(initQuizzes || null);

  const getQuiz = useCallback(
    (no: number): TQuiz | null => {
      if (!quizzes || !quizzes?.[no - 1]) return null;
      return quizzes[no - 1];
    },
    [quizzes]
  );

  const isCorrectAnswer = useCallback(
    (no: number, answer: string) => {
      return quizzes?.[no - 1]?.correct_answer === answer;
    },
    [quizzes]
  );

  const fetchQuestion = useCallback(async () => {
    setLoading(true);
    setError(false);
    setQuizzes(null);

    try {
      const response = await axios.get<{
        response_code: number;
        results: TResponseQuiz[];
      }>(`https://opentdb.com/api.php?amount=${quizCnt}&toke=my`);

      if (response.status === 200 && response.data.response_code === 0) {
        setQuizzes(
          response.data.results.map((d, i) => ({
            no: i + 1,
            question: d.question,
            correct_answer: d.correct_answer,
            choices: shuffle([...d.incorrect_answers, d.correct_answer]),
          }))
        );
        setLoading(false);
        setError(false);
      } else {
        setLoading(false);
        setError(true);
        setQuizzes(null);
      }
    } catch (e) {
      setQuizzes(null);
      setLoading(false);
      setError(true);
    }
  }, [quizCnt]);

  useEffect(() => {
    if (initQuizzes) return;

    fetchQuestion();
  }, [initQuizzes, fetchQuestion]);

  return {
    loading,
    error,
    quizzes,
    getQuiz,
    isCorrectAnswer,
  };
}

export default useQuiz;
