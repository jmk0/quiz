import { lazy, Suspense } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";

import Loading from "./components/Loading";
import { Wrapper } from "./lib/styles/styles";

const NotFoundPage = lazy(() => import("./page/NotFoundPage"));
const ResultPage = lazy(() => import("./page/ResultPage"));
const QuizPage = lazy(() => import("./page/QuizPage"));
const HomePage = lazy(() => import("./page/HomePage"));

function App() {
  return (
    <BrowserRouter>
      <Suspense
        fallback={
          <Wrapper>
            <Loading />
          </Wrapper>
        }
      >
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/quiz/:id" element={<QuizPage />} />
          <Route path="/result" element={<ResultPage />} />
          <Route path="*" element={<NotFoundPage />} />
        </Routes>
      </Suspense>
    </BrowserRouter>
  );
}

export default App;
