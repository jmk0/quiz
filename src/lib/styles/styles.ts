import styled from "styled-components";
import media from "./mediaQury";

export const ErrorArea = styled.div`
  display: flex;
  flex-direction: column;
  width: 100vw;
  height: 100vh;
  align-items: center;
  justify-content: center;

  > h1 {
    font-size: 3.5rem;
    font-weight: bold;
  }

  > p {
    margin-top: 1.8rem;
    line-height: 1.4;
    font-size: 1.8rem;
    text-align: center;
  }
`;

export const Wrapper = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const Button = styled.button`
  margin-top: 6rem;
  padding: 1rem 2rem;
  cursor: pointer;
  background: #00d69d;
  border: 0;
  border-radius: 1rem;
  font-size: 2rem;
  font-weight: 600;
  color: #fff;
  transition: opacity 0.2s;

  ${media.small} {
    padding: 1rem 1.5rem;
    margin-top: 5rem;
    font-size: 1.5rem;
  }

  &:hover {
    opacity: 0.8;
  }

  &[disabled] {
    cursor: default;
    background: #dadada;
    visibility: hidden;

    &:hover {
      opacity: 1;
    }
  }
`;
