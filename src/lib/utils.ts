import queryString from "query-string";

export function convertQueryString(object: any) {
  Object.keys(object).forEach((key) =>
    object[key] === undefined || object[key] === "" ? delete object[key] : {}
  );

  const stringified = queryString.stringify(object);
  return stringified;
}

export function shuffle(array: string[]) {
  return array.sort(() => Math.random() - 0.5);
}
