import { useMemo } from "react";
import styled from "styled-components";

import useRecords from "../hooks/useRecords";
import useQuiz from "../hooks/useQuiz";

import { TOTAL_QUIZ_COUNT } from "../lib/constant";
import type { TQuiz, TRecords } from "../@type";
import media from "../lib/styles/mediaQury";

type ResultContainerProps = {
  initQuizzes: TQuiz[];
  initRecords: TRecords[];
  timeToSolve: number;
};

function ResultContainer({
  initQuizzes,
  initRecords,
  timeToSolve,
}: ResultContainerProps) {
  const { quizzes } = useQuiz(TOTAL_QUIZ_COUNT, initQuizzes);
  const { userSolves } = useRecords(initRecords);

  const answerCnt = useMemo(() => {
    return quizzes?.filter(
      (q, i) => q?.correct_answer === userSolves?.[i]?.answer
    ).length;
  }, [quizzes, userSolves]);

  const incorrectAnswerCnt = useMemo(() => {
    return quizzes?.filter(
      (q, i) => q?.correct_answer !== userSolves?.[i]?.answer
    ).length;
  }, [quizzes, userSolves]);

  return (
    <dl>
      <Item>
        <dt>소요 시간</dt>
        <dd>{timeToSolve}초</dd>
      </Item>
      <Item>
        <dt>정답 개수</dt>
        <dd>{answerCnt}개</dd>
      </Item>
      <Item>
        <dt>오답 개수</dt>
        <dd>{incorrectAnswerCnt}개</dd>
      </Item>
    </dl>
  );
}

const Item = styled.div`
  display: flex;

  & + & {
    margin-top: 1rem;

    ${media.small} {
      margin-top: 0.8rem;
    }
  }

  > dt,
  > dd {
    font-size: 2rem;

    ${media.small} {
      font-size: 1.6rem;
    }
  }

  > dt {
    font-weight: bold;
  }

  > dd {
    margin-left: 1rem;

    ${media.small} {
      margin-left: 0.8rem;
    }
  }
`;

export default ResultContainer;
