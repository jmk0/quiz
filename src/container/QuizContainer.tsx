import { useCallback, useEffect, useMemo, useRef } from "react";
import { useLocation, useNavigate, useParams } from "react-router";
import queryString from "query-string";

import Loading from "../components/Loading";
import Quiz from "../components/Quiz";

import useRecords from "../hooks/useRecords";
import useQuiz from "../hooks/useQuiz";

import { Button } from "../lib/styles/styles";
import { SESSION_KEY, TOTAL_QUIZ_COUNT } from "../lib/constant";

function QuizContainer() {
  const navigate = useNavigate();
  const { id } = useParams();
  const no = Number(id);

  const location = useLocation();
  const { resolve } = queryString.parse(location.search);

  const startTimeInMs = useRef<number>(0);

  const initQuiz =
    resolve === "true"
      ? JSON.parse(sessionStorage.getItem(SESSION_KEY) || "")?.quizzes
      : null;

  const { loading, error, quizzes, getQuiz, isCorrectAnswer } = useQuiz(
    TOTAL_QUIZ_COUNT,
    initQuiz
  );

  const { userSolves, setUserAnswer } = useRecords();

  const quiz = useMemo(() => {
    return getQuiz(no);
  }, [getQuiz, no]);

  const isNext = useMemo(() => {
    return TOTAL_QUIZ_COUNT <= no ? false : true;
  }, [no]);

  const selectedChoice = useMemo(() => {
    return userSolves?.[no - 1]?.answer;
  }, [userSolves, no]);

  const isNextActive = useMemo(() => {
    return !!userSolves?.[no - 1];
  }, [userSolves, no]);

  const onClickChioce = useCallback(
    (choice: string) => {
      if (isNextActive) return;

      setUserAnswer(no, choice);
      if (isCorrectAnswer(no, choice)) alert("정답입니다.");
      else alert("오답입니다.");
    },
    [no, setUserAnswer, isCorrectAnswer, isNextActive]
  );

  const onNext = useCallback(() => {
    navigate(`/quiz/${no + 1}${location.search}`, { replace: true });
  }, [no, location.search, navigate]);

  const onResult = useCallback(() => {
    sessionStorage.setItem(
      SESSION_KEY,
      JSON.stringify({
        quizzes,
        records: userSolves,
        timeToSolve: (new Date().getTime() - startTimeInMs.current) / 1000,
      })
    );
    navigate("/result", { replace: true });
  }, [quizzes, userSolves, navigate]);

  useEffect(() => {
    startTimeInMs.current = new Date().getTime();
  }, []);

  return (
    <>
      {loading ? (
        <Loading />
      ) : error || !quiz ? (
        <div>Error</div>
      ) : (
        <Quiz
          {...quiz}
          totalNo={TOTAL_QUIZ_COUNT}
          selectedChoice={selectedChoice}
          onClick={onClickChioce}
        />
      )}

      {isNext ? (
        <Button type="button" disabled={!isNextActive} onClick={onNext}>
          다음
        </Button>
      ) : (
        <Button type="button" disabled={!isNextActive} onClick={onResult}>
          결과보기
        </Button>
      )}
    </>
  );
}

export default QuizContainer;
