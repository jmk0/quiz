import React from "react";
import styled from "styled-components";

import type { TQuiz } from "../../@type";
import media from "../../lib/styles/mediaQury";

type QuestionProps = {
  totalNo: number;
  selectedChoice?: string;
  onClick: (choice: string) => void;
} & Omit<TQuiz, "correct_answer">;

function Quiz({
  no,
  totalNo,
  question,
  choices,
  selectedChoice,
  onClick,
}: QuestionProps) {
  return (
    <Content>
      <Process>
        {no}/{totalNo}
      </Process>
      <Question dangerouslySetInnerHTML={{ __html: `Q. ${question}` }} />
      {choices.map((choice, idx) => (
        <Choice
          key={`${idx}-${choice}`}
          data-active={selectedChoice === choice}
          data-testid={`choice-element-${idx}`}
          onClick={() => onClick(choice)}
          dangerouslySetInnerHTML={{ __html: `${idx + 1}. ${choice}` }}
        />
      ))}
    </Content>
  );
}

const Content = styled.div`
  width: 52rem;

  ${media.small} {
    width: 100%;
    padding: 0 2.4rem;
    box-sizing: border-box;
  }
`;

const Process = styled.div`
  font-size: 2rem;
  margin-bottom: 1rem;
  color: #4c4a4c;

  ${media.small} {
    font-size: 1.4rem;
  }
`;

const Question = styled.div`
  margin-bottom: 4rem;

  font-size: 3rem;
  font-weight: 600;
  color: #4c4a4c;
  line-height: 1.2;

  ${media.small} {
    margin-bottom: 3.4rem;
    font-size: 1.8rem;
  }
`;

const Choice = React.memo(styled.div`
  font-size: 2rem;
  color: #4c4a4c;
  cursor: pointer;

  ${media.small} {
    font-size: 1.6rem;
  }

  &[data-active="true"] {
    font-weight: bold;
  }

  & + & {
    margin-top: 1rem;

    ${media.small} {
      font-size: 1.6rem;
      margin-top: 1.6rem;
    }
  }
`);

export default React.memo(Quiz);
