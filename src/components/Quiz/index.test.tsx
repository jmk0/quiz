import { fireEvent, render, screen } from "@testing-library/react";
import Quiz from "./index";
import quizzes from "../../../fixtures/quizzes";

describe("<Quiz />", () => {
  test("render quiz", () => {
    const handleClick = jest.fn();
    render(<Quiz {...quizzes[0]} onClick={handleClick} />);

    const questionText = screen.queryByText(
      "Q. What is the title of song on the main menu in Halo?"
    );
    const choice1Text = screen.queryByText(/[0-9]+. Opening/);
    const choice2Text = screen.queryByText(/[0-9]+. Shadows/);
    const choice3Text = screen.queryByText(/[0-9]+. Suite Autumn/);
    const choice4Text = screen.queryByText(/[0-9]+. Halo/);

    expect(questionText).toBeInTheDocument();
    expect(choice1Text).toBeInTheDocument();
    expect(choice2Text).toBeInTheDocument();
    expect(choice3Text).toBeInTheDocument();
    expect(choice4Text).toBeInTheDocument();
  });

  test("calls onClick prop when clicked", () => {
    const handleClick = jest.fn();
    render(<Quiz {...quizzes[0]} onClick={handleClick} />);

    fireEvent.click(screen.getByTestId("choice-element-0"));
    expect(handleClick).toHaveBeenCalledTimes(1);
  });
});
