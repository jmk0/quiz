import styled, { keyframes } from "styled-components";
import media from "../lib/styles/mediaQury";

function Loading() {
  return <Text data-testid="loading">LOADING...</Text>;
}

const fade = keyframes`
  50% { opacity: 0.5 }
  100%{ opacity: 1 }
`;

const Text = styled.span`
  font-weight: bold;
  font-size: 2rem;
  letter-spacing: 0.15rem;
  text-align: center;
  -webkit-animation: ${fade} 2s infinite;
  -moz-animation: ${fade} 2s infinite;

  ${media.small} {
    font-size: 1.6rem;
  }
`;

export default Loading;
