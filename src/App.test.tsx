import { render, screen, waitFor } from "@testing-library/react";

import App from "./App";

describe("<App /> ", () => {
  test("/ rendered", async () => {
    const { container } = render(<App />);

    const loadingElement = screen.getByTestId("loading");
    expect(container).toContainElement(loadingElement);

    await waitFor(() => {
      const buttonElement = screen.getByTestId("start");
      expect(container).toContainElement(buttonElement);
    });
  });
});
