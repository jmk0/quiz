import { useEffect } from "react";
import { useNavigate } from "react-router";
import styled from "styled-components";

import media from "../../lib/styles/mediaQury";

function HomePage() {
  let navigate = useNavigate();

  const handleClick = () => {
    navigate("/quiz/1");
  };

  useEffect(() => {
    sessionStorage.clear();
  }, []);

  return (
    <Content>
      <Button type="button" data-testid="start" onClick={handleClick}>
        퀴즈 풀기
      </Button>
    </Content>
  );
}

const Content = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100vw;
  height: 100vh;
`;

const Button = styled.button`
  background: #00d69d;
  border: 0;
  padding: 2.2rem 4rem;
  border-radius: 3rem;
  font-size: 3rem;
  font-weight: 600;
  color: #fff;
  cursor: pointer;
  transition: opacity 0.2s;

  &:hover {
    opacity: 0.8;
  }

  ${media.small} {
    font-size: 1.6rem;
    padding: 1.6rem 2.4rem;
    border-radius: 1.2rem;
  }
`;
export default HomePage;
