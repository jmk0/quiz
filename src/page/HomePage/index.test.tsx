import { render as renderRTL, screen } from "@testing-library/react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from ".";

const render = (ui: React.ReactNode) => {
  window.history.pushState({}, "", "/");
  return renderRTL(
    <BrowserRouter>
      <Routes>
        <Route path="/" element={ui} />
      </Routes>
    </BrowserRouter>
  );
};

describe("<HomePage />", () => {
  test("render home", () => {
    render(<HomePage />);

    const button = screen.queryByText("퀴즈 풀기");

    expect(button).toBeInTheDocument();
  });
});
