import { useCallback } from "react";
import { useNavigate } from "react-router";
import { Link } from "react-router-dom";
import styled from "styled-components";

import ResultContainer from "../container/ResultContainer";

import { SESSION_KEY } from "../lib/constant";
import media from "../lib/styles/mediaQury";

import { Button, ErrorArea, Wrapper } from "../lib/styles/styles";

function Result() {
  const navigate = useNavigate();
  const sessionInfo = JSON.parse(sessionStorage.getItem(SESSION_KEY) || "null");

  const reSolves = useCallback(() => {
    navigate("/quiz/1?resolve=true");

    sessionStorage.setItem(
      SESSION_KEY,
      JSON.stringify({
        quizzes: sessionInfo?.quizzes,
      })
    );
  }, [navigate, sessionInfo?.quizzes]);

  if (!sessionInfo)
    return (
      <ErrorArea>
        <h1>결과가 없습니다.</h1>
        <p>
          문제를 풀어주세요.
          <br />
          <Link to="/quiz/1">문제 풀기</Link>
        </p>
      </ErrorArea>
    );

  console.log(sessionInfo);
  return (
    <Wrapper>
      <Title>결과 보기</Title>
      <ResultContainer
        initQuizzes={sessionInfo?.quizzes}
        initRecords={sessionInfo?.records}
        timeToSolve={sessionInfo?.timeToSolve}
      />
      <ButtonArea>
        <Button type="button" onClick={reSolves}>
          다시 풀기
        </Button>
      </ButtonArea>
    </Wrapper>
  );
}

const Title = styled.h1`
  font-size: 3rem;
  font-weight: bold;
  margin-bottom: 4rem;

  ${media.small} {
    font-size: 2rem;
  }
`;

const ButtonArea = styled.div`
  button + button {
    margin-left: 0.8rem;
  }
`;

export default Result;
