import QuizContainer from "../container/QuizContainer";

import { Wrapper } from "../lib/styles/styles";

function QuizPage() {
  return (
    <Wrapper>
      <QuizContainer />
    </Wrapper>
  );
}

export default QuizPage;
