import { render, screen } from "@testing-library/react";
import NotFoundPage from "./index";

describe("<NotFoundPage />", () => {
  it("render NotFoundPage", () => {
    render(<NotFoundPage />);

    const h1 = screen.getByText("Not Found");
    const p = screen.getByText("Not Found");

    expect(h1).toBeInTheDocument();
    expect(p).toBeInTheDocument();
  });
});
