import { ErrorArea } from "../../lib/styles/styles";

function NotFound() {
  return (
    <ErrorArea>
      <h1>Not Found</h1>
      <p>페이지를 찾을 수 없습니다.</p>
    </ErrorArea>
  );
}

export default NotFound;
