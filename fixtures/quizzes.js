const quizzes = [
  {
    no: 1,
    totalNo: 2,
    question: "What is the title of song on the main menu in Halo?",
    choices: ["Opening Suite", "Shadows", "Suite Autumn", "Halo"],
  }
];

export default quizzes;